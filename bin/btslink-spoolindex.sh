#! /bin/bash

# Mirrors the Debian debbugs bugs database for use by bts-link
# (more details on accessing the DB at http://www.debian.org/Bugs/Access)

set -e

export LC_ALL=C

opts=q

if [ $# -lt 1 ]; then
	echo "$0 [-v] {spooldir}"
	exit 1
fi

spool="$1"
if [ "x$1" = "x-v" ]; then
    opts=v
    spool="$2"
fi

index=index.fwd
[ -n "$spool" -a -d "$spool" ] || exit 1

# mirror all .summary files of bugs
rsync -ad$opts rsync://bugs-mirror.debian.org/bts-spool-db/ \
    --include '*/*.summary' --exclude '*/*' --exclude '???*' $spool

# mirror the index of binary/source packages 
rsync -ad$opts bugs-mirror.debian.org:/srv/bugs.debian.org/etc/indices/sources $spool/source

# reconstruct index.fwd index of forwarded bugs
pushd $spool &>/dev/null
grep -Hr '^Forwarded-To' . | grep 'http[s]*://' | sed -e 's~^\(.*\)/\([0-9]*\).summary:Forwarded-To:~\2:~' | sort -n > $index.new
mv $index.new $index
popd &>/dev/null
