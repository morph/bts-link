# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   � 2012 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# @see http://confluence.atlassian.com/display/BITBUCKET/Issues

import urllib, urlparse, cgi, re, json, ssl

from BeautifulSoup import BeautifulSoup
from __init__ import *

class BitbucketData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Bitbucket: no id")

        # from the "visual" URL get the api URL
        uri = "https://api.bitbucket.org/1.0/repositories/%(user)s/%(project)s/issues/%(id)s" % id
        context = ssl.create_default_context(capath=CAPATH)
        data = json.load(urllib.urlopen(uri, context=context))

        self.status = data['status'] or failwith(uri, "Bitbucket", exn=NoStatusExn)
        self.resolution = None

        if self.status == 'duplicate':
            raise DupeExn(uri)


class RemoteBitbucket(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, BitbucketData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"https?://(?:www.)?bitbucket.org/(?P<user>.*)/(?P<project>.*)/issue/(?P<id>[0-9]+).*")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, bugId):
        return "http://bitbucket.org/%(user)s/%(project)s/issue/%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('resolved', 'closed')

    def isWontfix(self, status, resolution):
        return status in ('wontfix',)


RemoteBts.register('bitbucket', RemoteBitbucket)
