# vim:set encoding=utf-8:

# Copyright:
#   © 2009 Sanghyeon Seo <sanxiyn@gmail.com>
#
# License:
#   Public Domain

# @see http://confluence.atlassian.com/display/JIRA/JIRA+RPC+Services

# JIRA seems to require a user account to use its SOAP service.
# Therefore, you need to create a user with ID bts-link, PW bts-link
# before adding an entry to btslink.cfg. Sorry, this can't be automated.
#
# ST, 2009-11-05: ^^ really? apache seems not to need this.

import SOAPpy

from __init__ import *
from base import die
from secrets import SECRETS

class JIRAData:
    def __init__(self, bts, uri):
        
        id = bts.extractBugid(uri)
        if not id: return None

        # initialize soap (in case we're first bug in the queue on that remote)
        bts.initSoap()
        issue = bts.soap.getIssue(bts.auth, id)

        self.id = id or failwith(uri, 'JIRA: no id')
        self.status = bts.status_map[issue.status]
        self.resolution = None

        if issue.resolution:
            self.resolution = bts.resolution_map[issue.resolution]
            if issue.resolution == 'Duplicate':
                raise DupeExn(uri)

class RemoteJIRA(RemoteBts):
    def __init__(self, cnf):
        bugre = r'^%(uri)s/browse/([A-Z]+-[0-9]+)$'
        urifmt = '%(uri)s/browse/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, JIRAData)

    def initSoap(self):
        # no need to do anything if we've already set up a proxy and have succeeded in authenticating
        if hasattr(self, 'soap') and hasattr(self, 'auth'):
            return
        
        uri = self._cnf['uri']
        wsdl = uri + '/rpc/soap/jirasoapservice-v2?wsdl'
        
        # We may have previously set up the proxy but need to authenticate again
        if not hasattr(self, 'soap'):
            try:
                self.soap = SOAPpy.WSDL.Proxy(wsdl)
            except:
                failwith(wsdl, "Error accessing Jira WSDL Proxy", exn=ConnectionFailedExn)

        secretentry = self._cnf['type'] + ':' + uri
        password = SECRETS.get(secretentry, 'bts-link')
        try:
            self.auth = self.soap.login('bts-link', password)
        except SOAPpy.Types.faultType as e:
            # Make sure to notify which Jira server refused login.
            failwith(uri, str(e), exn=ConnectionFailedExn)

        self.status_map = {}
        for status in self.soap.getStatuses(self.auth):
            self.status_map[status.id] = status.name

        self.resolution_map = {}
        for resolution in self.soap.getResolutions(self.auth):
            self.resolution_map[resolution.id] = resolution.name

    def _getReportData(self, uri):
        
        data = JIRAData(self, uri)
        return data

    def isClosing(self, status, resolution):
        return status in ('Resolved', 'Closed')

    def isWontfix(self, status, resolution):
        return resolution in ("Won't Fix",)

RemoteJIRA.register('jira', RemoteJIRA)
