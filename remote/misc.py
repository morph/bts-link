# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2009 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# This file will contain misc project issue tracking systems
# not abstract enough to warrant a separate file


### PHP ###
#
# TODO: support PEAR
#       PEAR xml link: http://pear.php.net/bugs/rss/bug.php?format=xml&id=2415

import urllib#, urlparse, cgi
import ssl

#from BeautifulSoup import BeautifulSoup
from __init__ import *

try:
    # if in python 2.5
    from xml.etree import ElementTree as ET
except ImportError:
    # this is for python 2.4 (external module)
    from elementtree import ElementTree as ET


class PhpData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Php: no id")

        context = ssl.create_default_context(capath=CAPATH)

        # http://www.learningpython.com/2008/05/07/elegant-xml-parsing-using-the-elementtree-module/
        if 'php.net' in uri:
            feed = urllib.urlopen(uri.replace(self.id, '').replace('bug.php?id=', '') + "/rss/bug.php?format=xml&id=" + self.id, context=context)
        elif 'bugs.mysql.com' in uri:
            feed = urllib.urlopen(uri.replace(self.id, '').replace('bug.php?id=', '') + "/bug.php?format=xml&id=" + self.id, context=context)
        tree = ET.parse(feed)

        # find the element 'status', and get its text
        self.status = tree.find('status').text
        self.resolution = None

        if self.status == 'Duplicate':
            raise DupeExn(uri)

class RemotePhp(RemoteBts):
# lists of status-es
# http://cvs.php.net/viewvc.cgi/php-bugs-web/include/functions.inc?view=markup#l82
    def __init__(self, cnf):

        urifmt = '%(uri)s%(id)s'

        RemoteBts.__init__(self, cnf, cnf['uri'], urifmt, PhpData)

    def isClosing(self, status, resolution):
        return status in ('Closed',)

    def isWontfix(self, status, resolution):
        return status in ('Wont-fix', 'Bogus')

RemoteBts.register('php', RemotePhp)
