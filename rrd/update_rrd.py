#!/usr/bin/python
# vim:set encoding=utf-8:
###############################################################################
# Update RRD with the last execution summary
#
# Docs used:
# http://oss.oetiker.ch/rrdtool/doc/rrdupdate.en.html
###############################################################################
# Copyright:
#   © 2015 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

from __future__ import with_statement
import rrdtool
import os.path
import sys
from collections import defaultdict
import yaml

if len(sys.argv) == 1:
    print "Usage: " + sys.argv[0] + " <summary files>"
    sys.exit(1)

basedir = os.path.dirname(os.path.abspath(__file__))
rrdfile = os.path.join(basedir, 'bts-link.rrd')

r = defaultdict(lambda: defaultdict(dict))

with open(sys.argv[1]) as f:
    summary = yaml.load(f)
    # rrd wants epoch as timestamps
    summary_epoch = summary['Execution complete'].strftime('%s')
    r[summary_epoch]['elapsed_time'] = int(summary['Elapsed time'])
    for tag in summary['Tags summary']:
        r[summary_epoch][tag] = summary['Tags summary'][tag]

for ts in sorted(r.keys()):
    rrdtool.update(rrdfile,
        '-t', ':'.join(sorted(r[ts].keys())),
        '%s:%s' % (ts, ':'.join(str(r[ts][k]) for k in sorted(r[ts].keys())))
        )